﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IdentityModel.Tokens.Jwt;
using System.IdentityModel.Tokens;
using Microsoft.IdentityModel.Tokens;

namespace jwt_example
{
    class Program
    {
        static void Main(string[] args)
        {
            const string securityWord = "secret";
            byte[] securityKeyByteArray = Encoding.Default.GetBytes(securityWord);

            Console.WriteLine("security word : " + securityWord);
            Console.WriteLine("security word convert byte array length: " + securityKeyByteArray.Length);

            if (securityKeyByteArray.Length < 64)
            {
                Console.WriteLine("ZeroFill security key");

                securityKeyByteArray = new byte[64];
                System.Buffer.BlockCopy(Encoding.Default.GetBytes(securityWord), 0, securityKeyByteArray, 0, Encoding.Default.GetBytes(securityWord).Length);
            }

            var securityKey = new SymmetricSecurityKey(securityKeyByteArray);
            var signingCredentials = new SigningCredentials(
                securityKey,
                SecurityAlgorithms.HmacSha256);

            var header = new JwtHeader(signingCredentials);

            var now = DateTime.UtcNow;
            var payload = new JwtPayload
            {
                {"iss", "a5fgde64-e84d-485a-be51-56e293d09a69"},
                {"scope", "https://example.com/ws"},
                {"aud", "https://example.com/oauth2/v1"},
                {"iat", now},
            };

            var secToken = new JwtSecurityToken(header, payload);

            var handler = new JwtSecurityTokenHandler();
            var tokenString = handler.WriteToken(secToken);
            Console.WriteLine(tokenString);
        }

        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public static byte[] FromHex(string hex)
        {
            hex = hex.Replace("-", "");
            byte[] raw = new byte[hex.Length / 2];
            for (int i = 0; i < raw.Length; i++)
            {
                raw[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            }
            return raw;
        }
    }
}
